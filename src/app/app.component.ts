import { Component, OnInit, Injectable, AfterViewInit } from '@angular/core';

@Component({
    selector: 'app',
    template: require('./app.component.html')
})
export class AppComponent implements OnInit {

    selIndex: number;

    constructor() { }

    ngOnInit() {
        // workaround para evitar bug na renderização do mdTab
        setTimeout(() => this.initIndex(), 0);
    }

    private initIndex() {
      this.selIndex = 0;
    }
}
