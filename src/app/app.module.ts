import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { MaterialModule, MdButton } from '@angular/material';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        MaterialModule
    ],
    declarations: [
        AppComponent
    ],
    providers: [ ],
    bootstrap: [AppComponent]
})
export class AppModule { }