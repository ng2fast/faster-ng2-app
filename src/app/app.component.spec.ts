import { AppComponent } from './app.component';
import { ComponentTestingHelper } from '../testing/helpers/component-testing.helper';


describe('AppComponent', () => {
    let helper: ComponentTestingHelper<AppComponent>;
    beforeEach(() => {
        helper = ComponentTestingHelper.init(AppComponent);
        helper.detectChanges();
    });
    it('should be initialized', () => {
        expect(helper.instance).toBeDefined();
    });
    it('starts with selectedIndex equals 0', () => {
        helper.instance['initIndex']();
        expect(helper.instance.selIndex).toEqual(0);
    });
});
