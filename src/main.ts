
import "./main.scss";

import 'core-js/es7/reflect';
// Angular wants it
import 'zone.js/dist/zone';

import 'hammerjs';


import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app/app.module';
import { enableProdMode } from '@angular/core';

// Styles

const isProduction = process.env.NODE_ENV == 'production' ? true : false;

const environment = { production: isProduction };



if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule);

