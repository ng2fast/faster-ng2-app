import { TestBed, ComponentFixture, getTestBed } from '@angular/core/testing';
import { Type, DebugElement } from '@angular/core';
import { AppModule } from '../../app/app.module';

export class ComponentTestingHelper<T> {
    static init<T>(componentClass: Type<T>, providers: any[] = []) {
        let fixture = TestBed.configureTestingModule({
            imports: [AppModule],
            providers: providers

        }).createComponent<T>(componentClass);
        return new ComponentTestingHelper<T>(fixture);
    }

    constructor(private fixture: ComponentFixture<T>) {

    }

    get instance() {
        return this.fixture.componentInstance;
    }

    detectChanges() {
        this.fixture.detectChanges(false);
    }

    get debugElement(): DebugElement {
        return this.fixture.debugElement;
    }

    getProvider<U>(type: Type<U>): U {
        return getTestBed().get(type);
    }
}