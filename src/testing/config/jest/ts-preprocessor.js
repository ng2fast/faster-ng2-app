
const tsc = require('typescript');
const tsConfig = {
  "compilerOptions": {
    "allowSyntheticDefaultImports": true,
    "declaration": false,
    "emitDecoratorMetadata": true,
    "experimentalDecorators": true,
    "lib": [
      "dom",
      "es2015"
    ],
    "module": "commonjs",
    "moduleResolution": "node",
    "sourceMap": true,
    "target": "es5"
  },
  "include": [
    "./**/*.ts",
    "../../rx-operators.ts",
    "../../declarations.d.ts"
  ],
  "exclude": [
    "node_modules"
  ],
}

const fs = require('fs');

module.exports = {

  process(src, path) {
    if (path.endsWith('.ts')) {
      return tsc.transpile(
        src,
        tsConfig.compilerOptions,
        path,
        []
      );
    }
    return src;
  },
};
